#!/bin/bash

set -e

mkdir -p /home/phablet/.config/upstart/

cp -v /opt/click.ubuntu.com/datamonitor.matteobellei/current/daemon/mattdaemon.conf /home/phablet/.config/upstart/

cp -v /opt/click.ubuntu.com/datamonitor.matteobellei/current/daemon/mattdaemon-service.conf /home/phablet/.config/upstart/

echo "dataMonitor daemon installed!"
