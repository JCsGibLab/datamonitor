cmake_minimum_required(VERSION 3.5)

set(STATLIB "Actiondaemon")

find_package(Qt5Core)

add_executable(${STATLIB} actiondaemon.cpp networkdaemon.cpp)
target_link_libraries(${STATLIB})

qt5_use_modules(${STATLIB} Qml Quick DBus)
