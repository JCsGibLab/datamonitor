#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "readconns.h"

void ReadconnsPlugin::registerTypes(const char *uri) {
    //@uri Pluginname
    qmlRegisterSingletonType<Readconns>(uri, 1, 0, "Readconns", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Readconns; });
}
