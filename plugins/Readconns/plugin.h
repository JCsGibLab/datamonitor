#ifndef READCONNSPLUGIN_H
#define READCONNSPLUGIN_H

#include <QQmlExtensionPlugin>

class ReadconnsPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
