
/*
    Contains ALL the function used to manage the application database
*/

  /* See: http://doc.qt.io/archives/qt-5.5/qtquick-localstorage-qmlmodule.html for input arguments */
  function getDatabase(connType) {
     if (connType=="WIFI") {
       return LocalStorage.openDatabaseSync("dataMonitor_db", "1.0", "StorageDatabase", 1000000);
     } else if (connType=="SIM") {
       return LocalStorage.openDatabaseSync("dataMonitor_db_SIM", "1.0", "StorageDatabase", 1000000);
     }
  }


  /* create the necessary tables.
     Note: with a column of type REAL for the saved value, is always used the comma as decimal separator also when user uses the dot sign */
   function createTables(connType) {

      var db = getDatabase(connType);

      db.transaction(
          function(tx) {
              tx.executeSql('CREATE TABLE IF NOT EXISTS bytesreceived(id INTEGER PRIMARY KEY AUTOINCREMENT, date_column TEXT, bytes_column REAL)');
      });
   }

  function getBytesValueByDate(date, connType){

        var db = getDatabase(connType);
        var targetDate = new Date (date);

        /* return a formatted date like: 2017-04-30 (yyyy-mm-dd) */
        var fullTargetDate = DateUtils.formatDateToString(targetDate);
        var rs = "";

        db.transaction(function(tx) {
              rs = tx.executeSql("SELECT bytes_column FROM bytesreceived t where date(t.date_column) = date('"+fullTargetDate+"')");
            }
        );

        /* check if value is missing or not */
        if (rs.rows.length > 0) {
            var row = rs.rows.length -1;
            return rs.rows.item(row).bytes_column;
        } else {
            return 0;
        }
   }

  function getBytesValueByDateAndPos(date, position, connType){

          var db = getDatabase(connType);
          var targetDate = new Date (date);

          /* return a formatted date like: 2017-04-30 (yyyy-mm-dd) */
          var fullTargetDate = DateUtils.formatDateToString(targetDate);
          var rs = "";

          db.transaction(function(tx) {
                rs = tx.executeSql("SELECT bytes_column FROM bytesreceived t where date(t.date_column) = date('"+fullTargetDate+"')");
              }
          );

          /* check if value is missing or not */
          if (rs.rows.length > 0) {
              var row = position;
              return rs.rows.item(row).bytes_column;
          } else {
              return 0;
          }
     }

  function getTodayLastPosition(date, connType){

          var db = getDatabase(connType);
          var targetDate = new Date (date);

          /* return a formatted date like: 2017-04-30 (yyyy-mm-dd) */
          var fullTargetDate = DateUtils.formatDateToString(targetDate);
          var rs = "";

          db.transaction(function(tx) {
                rs = tx.executeSql("SELECT bytes_column FROM bytesreceived t where date(t.date_column) = date('"+fullTargetDate+"')");
              }
          );

          /* check if value is missing or not */
          if (rs.rows.length > 0) {
              var row = rs.rows.length -1;
              return row;
          } else {
              return 0;
          }
     }

     function insertBytesData(date, bytesValue, connType){

       var db = getDatabase(connType);
       var fullDate = new Date (date);
       var res = "";

       /* return a formatted date like: 2017-09-30 (yyyy-mm-dd) */
       var dateFormatted = DateUtils.formatDateToString(fullDate);

       db.transaction(function(tx) {

           var rs = tx.executeSql('INSERT INTO bytesreceived(date_column, bytes_column) VALUES (?,?);', [dateFormatted, bytesValue]);
           if (rs.rowsAffected > 0) {
               res = "OK";
           } else {
               res = "Error";
           }
       }
       );
       return res;
  }
